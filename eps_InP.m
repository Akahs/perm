% Author: Kaijun Feng
% Date: 5/31/2015
% Notes: this funciton is used to calculate the permittivity of InP. There
% are two models available: the classical lorentzian model and the
% factorial model. The two models are almost identical except for the minor
% difference of the damping constants. Plasma term is not added in the
% model yet.
% Ref: Solid State Comm. vol 136, pp. 404 (2005)
function [re, im]=eps_InP(omega, n)
omega_LO = 345; % 1/cm
omega_TO = 304;
% gamma_LO = 0.95;
% gamma_TO = 2.8;
eps_inf = 9.61;
gamma = 3;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega.^2+1i*omega*gamma));
% Factorial model
% eps = eps_inf*(omega_LO^2-omega.^2+1i*gamma_LO*omega)./(omega_TO^2-omega.^2+1i*gamma_TO*omega);
re = real(eps);
im = -imag(eps);
end