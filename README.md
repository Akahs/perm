# README #



### What is this repository for? ###

* This is a personal project for generating permittivity for common solid materials, especially in the mid-infrared spectral range. It can also be used to investigate composite material with layered structures (hyperbolic metamaterial or multiple quantum well). Howeve no quantum effects are included in the calculation of permittivity -- only the plasmonic and phononic effects. The material "composite" in uniaxial material group is made of layered InGaAs and AlInAs structure with 50% filling ratio. n_InGaAs = 1e18 cm^-3.
* Version:1.2

### How do I get set up? ###

* Download source code and run the file: PermTool_v1.m. To simplify future use, use the MATLAB compiler to convert the project into an executable. MATLAB runtime will be installed then.
* Configuration N/A
* Dependencies: general MATLAB library and all included source code.
* Database configuration N/A
* How to run tests: PermTool_v1.m.
* Deployment instructions N/A

### Contribution guidelines ###

* Please fork the repository for any potential contributions
* Code review: file a pull request
* Other guidelines N/A

### Who do I talk to? ###

* For any question, please contact the repository owner or email kfeng@nd.edu.
* You may also contact the Mid-infrared & THz Photonics group at University of Notre Dame for further questions.