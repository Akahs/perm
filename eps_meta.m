% A simple function for calculating permittivity of InGaAs/AlInAs
% metamaterial, n = 8.3e18
function eps=eps_meta(omega)
[r1,i1] = eps_InGaAs(omega,8.3e18);
[r2,i2] = eps_AlInAs(omega,0);
eps1 = r1 - 1i * i1;
eps2 = r2 - 1i * i2;
eps = 2*eps1*eps2/(eps1+eps2);