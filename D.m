function Y = D(X)
    n = length(X);
    Y = zeros(1,n);
    fun = @(t,s) exp(t.^2-s.^2);
    for k = 1:n
        Y(k) = integral(@(t)fun(t,X(k)),0,X(k));
    end
end