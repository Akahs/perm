% Permittivity of GaN14 with plasma term considered 
% Damping constant was modeled based on linear fitting results (source
% data: H. Harima, et al., MSF vol. 264-268 (1998), pp. 1363)
function [y1,y2]=GaN14eps_doped(omega,n)
c=3e10; %speed of light
q=4.8e-10; %statC
m=0.2*9.1e-28; %effective mass in g
omega_L=735; %1/cm
omega_T=556;
eps_inf=5.35;
omega_p=sqrt((4*pi*n*q^2)/(m*eps_inf));
omega_p=omega_p/(2*pi*c);
Gamma=9;
gamma=1.44e-5*n^0.4; %curve fitting result
% gamma=1;
eps=eps_inf*(1+(omega_L^2-omega_T^2)/(omega_T^2-omega^2+1i*omega*Gamma)-omega_p^2/(omega^2-1i*omega*gamma));
y1=real(eps);
y2=-imag(eps);
end
