% Author: Kaijun Feng
% Date: 6/18/2015
% Notes: this funciton is used to calculate the permittivity of GaP. Plasma term is not added in the
% model yet.
% Ref: Sol. Stat. Commun. 136, 404 (2005)
function [re, im]=eps_cBN(omega, n)
omega_LO = 1305; % 1/cm
omega_TO = 1055;
% gamma_L = 1.18;
% gamma_T = 2.59;
gamma = 0.04;
eps_inf = 4.5;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega^2+1i*omega*gamma));
% Factorial model
% eps = eps_inf*(omega_LO^2-omega^2+1i*gamma_L*omega)/(omega_TO^2-omega^2+1i*gamma_T*omega);
re = real(eps);
im = -imag(eps);
end