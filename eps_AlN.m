% Author: Kaijun Feng
% Date: 6/18/2015
% Notes: this funciton is used to calculate the permittivity of GaP. Plasma term is not added in the
% model yet.
% Ref: Appl. Phys. Lett. 86, 141912 (2005), Phys. Rev. B 28, 4579 (1983)
function [re, im]=eps_AlN(omega, n)
omega_LO = 893; % 1/cm
omega_TO = 667;
gamma = 2.2;
eps_inf = 4.76;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega.^2+1i*omega*gamma));
re = real(eps);
im = -imag(eps);
end