% Author: Kaijun Feng
% Date: 6/2/2016
% Note: permittivity for sapphire
% Ref: [1] M. Schubert, et al. Phys. Rev. B 61, pp. 8187 (2000)
function [re, im] = eps_sapphire_e (omega,n)
    omega_T = [398 582];
    omega_L = [511 881];
    gamma_T = [5.3 3.0];
    gamma_L = [1.1 15.4];
    eps_inf = 3.072;
    eps = eps_inf;
    for j=1:2
        eps = eps * (omega_L(j)^2-omega^2 - 1i*omega*gamma_L(j))/...
            (omega_T(j)^2-omega^2 - 1i*omega*gamma_T(j));
    end
    re = real(eps);
    im = imag(eps);
end
