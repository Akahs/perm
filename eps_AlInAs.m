% Author: Kaijun Feng
% Last edited: 6/1/2015
% Note: permittivity for AlInAs. Plasmon term not considered.
% Ref: [1] J. Phys. C: Solid State Phys. 16, L579 (1983)
% [2] Solid State Comm. 136, 404 (2005)
function [re, im] = eps_AlInAs(omega,n)
eps_inf = 9.82; % definition of all parameters similar to eps_InGaAs
omega_TO_InAs = 224;
omega_LO_InAs = 236;
gamma_InAs = 4;
omega_TO_AlAs = 346;
omega_LO_AlAs = 369;
gamma_AlAs = 7;
f1 = (omega_LO_AlAs^2-omega_TO_InAs^2)/(omega_TO_AlAs^2-omega_TO_InAs^2); % oscillator strength
eps_phonon1 = f1*(omega_LO_InAs^2-omega_TO_InAs^2)/(omega_TO_InAs^2-omega^2-1i*omega*gamma_InAs);
f2 = (omega_TO_AlAs^2-omega_LO_InAs^2)/(omega_TO_AlAs^2-omega_TO_InAs^2);
eps_phonon2 = f2*(omega_LO_AlAs^2-omega_TO_AlAs^2)/(omega_TO_AlAs^2-omega^2-1i*omega*gamma_AlAs);
eps = eps_inf*(1+eps_phonon1+eps_phonon2);
re = real(eps);
im = imag(eps);
end
