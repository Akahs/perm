% Author: Kaijun Feng
% Date: 3/9/2016
% Notes: this funciton is used to calculate the permittivity of Germanium.
% Only valid for IR range. Needs better model.
function [re, im]=eps_Ge(omega, n)
eps = 16;
re = real(eps);
im = -imag(eps);
end