% Author: Kaijun Feng
% Date: 6/19/2015
% Notes: this funciton is used to calculate the permittivity of AlAs. Plasma term is not added in the
% model yet.
% Ref: Solid State Comm. 136, 404 (2005)
function [re, im]=eps_AlAs(omega, n)
omega_LO = 402; % 1/cm
omega_TO = 362;
% gamma_L = 1.18;
% gamma_T = 2.59;
gamma = 4;
eps_inf = 8.2;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega^2+1i*omega*gamma));
% Factorial model
% eps = eps_inf*(omega_LO^2-omega^2+1i*gamma_L*omega)/(omega_TO^2-omega^2+1i*gamma_T*omega);
re = real(eps);
im = -imag(eps);
end