% Author: Kaijun Feng
% Date: 6/18/2015
% Notes: this funciton is used to calculate the permittivity of InSb. Plasma term is not added in the
% model yet.
% Ref: Solid State Comm. 136, 404 (2005)
function [re, im]=eps_InSb(omega, n)
omega_LO = 190; % 1/cm
omega_TO = 179;
% gamma_L = 1.18;
% gamma_T = 2.59;
gamma = 3;
eps_inf = 15.7;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega^2+1i*omega*gamma));
% Factorial model
% eps = eps_inf*(omega_LO^2-omega^2+1i*gamma_L*omega)/(omega_TO^2-omega^2+1i*gamma_T*omega);
re = real(eps);
im = -imag(eps);
end