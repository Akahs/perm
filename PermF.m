% MIT License
% 
% Copyright (c) 2016 Kaijun Feng
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
% 
% Author: Kaijun Feng
% Date: 5/28/2015
% Notes: This function is used to generate the permitivitty data for given
% materials, with the auxiliary functions for each material.
function PermF(handles)
%% set up
global xmin xmax xtype rho
N=500;
switch xtype
    case 1 % wavenumber
        omega=linspace(xmin,xmax,N);
        xvalue = omega;
    case 2 % wavelength
        lambda=linspace(xmin,xmax,N);
        omega = 1e4./lambda;
        xvalue = lambda;
    case 3 % frequency in THz
        c = 2.998e10; % cm/s
        f = linspace(xmin,xmax,N); % in THz
        omega = 1e12 * f / c;
        xvalue = f;
end
hwb = waitbar(0,'Calculating...','Name','Perm rocks');
%% Calculating epsilon
funs = {'eps_InGaAs', 'GaN14eps_doped','eps_GaAs', 'eps_AlInAs', 'eps_InP',...
    'eps_AlN','eps_GaP','eps_cBN','eps_GaSb','eps_InSb','eps_InAs','eps_AlAs',...
    'eps_glass','eps_Ag','eps_Ge','eps_SiC','eps_Au','eps_Al'};
funs_uni = {'eps_sapphire_o','eps_sapphire_e','eps_composite_o','eps_composite_e'};
advanced = get(handles.tog1, 'Value');
uniaxial = get(handles.radio_uni,'Value');
if uniaxial
    eps_o_tmp = zeros(2,N);
    eps_e_tmp = zeros(2,N);
    n = get(handles.slider1,'Value');
    identifier = get(handles.pop_uni,'Value');
    for k=1:N
        [eps_o_tmp(1,k), eps_o_tmp(2,k)]=feval(funs_uni{identifier*2-1}, omega(k), n);
        [eps_e_tmp(1,k), eps_e_tmp(2,k)]=feval(funs_uni{identifier*2}, omega(k), n);
        waitbar(k/N,hwb);
    end
    eps_o = eps_o_tmp(1,:) + 1i*eps_o_tmp(2,:);
    eps_e = eps_e_tmp(1,:) + 1i*eps_e_tmp(2,:);
    if advanced % MQW embedded in HMM
        eps_o_uni = eps_o;
        eps_e_uni = eps_e;
        eps2 = zeros(2,N);
        n2 = get(handles.edit2,'String');
        n2 = str2double(n2);
        identifier2 = get(handles.pop2, 'Value');
        for k=1:N
            [eps2(1,k), eps2(2,k)]=feval(funs{identifier2}, omega(k), n2);
            waitbar(k/N,hwb);
        end
        eps2_temp=eps2(1,:)+1i*eps2(2,:);
        eps_o=(1-rho)*eps_o_uni+rho*eps2_temp;
        eps_e=1./(rho./eps2_temp+(1-rho)./eps_e_uni);
    end
elseif advanced % simple HMM
    eps1 = zeros(2,N);
    eps2 = zeros(2,N);
    n1 = get(handles.slider1,'Value');%cm^-3
    n2 = get(handles.edit2,'String');
    n2 = str2double(n2);
    identifier1 = get(handles.pop1, 'Value');
    identifier2 = get(handles.pop2, 'Value');
    for k=1:N
        [eps1(1,k), eps1(2,k)]=feval(funs{identifier1}, omega(k), n1);
        [eps2(1,k), eps2(2,k)]=feval(funs{identifier2}, omega(k), n2);
        waitbar(k/N,hwb);
    end
    eps1_temp=eps1(1,:)+1i*eps1(2,:);
    eps2_temp=eps2(1,:)+1i*eps2(2,:);
    eps_o=(1-rho)*eps1_temp+rho*eps2_temp;
    eps_e=1./(rho./eps2_temp+(1-rho)./eps1_temp);
else % simple isotropic       
    eps = zeros(2,N);
    identifier = get(handles.pop1, 'Value');
    n = get(handles.slider1,'Value');
    for k=1:N
        [eps(1,k), eps(2,k)]=feval(funs{identifier}, omega(k), n);
        waitbar(k/N,hwb);
    end
end
close(hwb);
%% plot
realPart = strcmp(get(handles.realPart,'Checked'), 'on');
imagPart = strcmp(get(handles.imagPart,'Checked'), 'on');
ordPart = strcmp(get(handles.ordPart,'Checked'), 'on');
extPart = strcmp(get(handles.extPart,'Checked'), 'on');
h=handles.axes1;
fsz=12;
lw=1;
hl=legend();
% set(hl,'Interpreter','latex');
if advanced || uniaxial
    if realPart
        if extPart
            plot(h, xvalue, real(eps_e),'LineWidth',lw,'DisplayName','Real(eps_e)');
            hold on;
        end
        if ordPart
            plot(h, xvalue, real(eps_o),'LineWidth',lw,'DisplayName','Real(eps_o)');
            hold on;
        end
    end
    if imagPart
        if extPart
            plot(h, xvalue, imag(eps_e),'--','LineWidth',lw,'DisplayName','Imag(eps_e)');
            hold on;
        end
        if ordPart
            plot(h, xvalue, imag(eps_o),'--','LineWidth',lw,'DisplayName','Imag(eps_o)');
            hold on;
        end
    end 
    hold off;
    legend('off');
    legend('show');
    switch xtype
        case 1
            xlabel('Frequency(cm^{-1})');
        case 2
            xlabel('Wavelength(\mu m)');
        case 3
            xlabel('Frequency(Hz)');
    end
    ylabel('Real($\varepsilon$)','Interpreter','latex');
else    
    if realPart
        plot(h, xvalue,eps(1,:),'LineWidth',lw,'DisplayName','Real(eps)');
        hold on; 
    end
    if imagPart
        plot(h, xvalue,eps(2,:),'--','LineWidth',lw,'DisplayName','Imag(eps)');
        hold on;
    end
    hold off;
    legend('off');
    legend('show');
%     set(hl,'Interpreter','latex');
    switch xtype
        case 1
            xlabel('Frequency(cm^{-1})');
        case 2
            xlabel('Wavelength(\mu m)');
        case 3
            xlabel('Frequency(THz)');
    end
    ylabel('Permittivity');
end
% set(h,'Paeosition',[0 0 3.37 2.4]');
set(gca,'FontSize',fsz,'FontName','Times New Roman');
% legend('real part','imag part');
grid_status = get(handles.grid,'Checked');
if strcmp(grid_status,'on')
    grid on;
end
xlim([xmin xmax]);
% ylim([-300 600]);
% print(h,'eps_zoom_in','-depsc','-r300');

end