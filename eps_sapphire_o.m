% Author: Kaijun Feng
% Date: 6/2/2016
% Note: permittivity for sapphire
% Ref: [1] M. Schubert, et al. Phys. Rev. B 61, pp. 8187 (2000)
function [re, im] = eps_sapphire_o (omega,n)
    omega_T = [385 439 569 634];
    omega_L = [388 482 630 907];
    gamma_T = [3.3 3.1 4.7 5.0];
    gamma_L = [3.1 1.9 5.9 14.7];
    eps_inf = 3.077;
    eps = eps_inf;
    for j=1:4
        eps = eps * (omega_L(j)^2-omega^2 - 1i*omega*gamma_L(j))/...
            (omega_T(j)^2-omega^2 - 1i*omega*gamma_T(j));
    end
    re = real(eps);
    im = imag(eps);
end
