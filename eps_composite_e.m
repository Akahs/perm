% Author: Kaijun Feng
% Date: 3/17/2017
% Note: permittivity for the composite material consisting of InGaAs and
% AlInAs, assuming 50% filling ratio.
function [re, im] = eps_composite_e (omega,n)
    [eps1R, eps1I] = eps_InGaAs(omega,1e18);
    [eps2R, eps2I] = eps_AlInAs(omega,0);    
    eps1 = eps1R+1i*eps1I; 
    eps2 = eps2R+1i*eps2I;
    eps = (2 * eps1 * eps2)/(eps1 + eps2);
    re = real(eps);
    im = imag(eps);
end
