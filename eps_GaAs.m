% Author: Kaijun Feng
% Date: 6/17/2015
% Note: permittivity for GaAs, with phonon and plasmon both considered.
% Ref: [1] R. T. Holm, et al., J. Appl. Phys. 48, 212 (1977)
% [2] A. Raymond, et al., J. Phys. C 12, 2289 (1979)
% [3] W. J. Moore, et al., J. Appl. Phys. 80, 6939 (1996)
% [4] S. Perkowitz, et al., Sol. Stat. Comm. 16, 1093 (1975)
function [re, im] = eps_GaAs(omega,n)
%% effective mass
n = n*1e6; % m^-3
c = 3e8; %m/s
m0 = 9.11e-31; % kg
m0s = 0.067 * m0;
h_bar = 1.05e-34; % J*s
phi = 0.8156*(3*pi^2)^(2/3)*(h_bar^2/(2*m0s))*n.^(2/3);
Eg = 1.52*1.6e-19; % J
ms = m0s./(1-2*phi/Eg);
n = n*1e-6; % cm^-3

%% plasma freq
c=3e10; %speed of light
q=4.8e-10; %statC
eps_inf = 10.88;
ms = 1e3*ms; %g
omega_p = sqrt(4*pi*n*q^2/(ms*eps_inf)); %rad/s
omega_p = omega_p/(2*pi*c);%1/cm

%% dielectric function
omega_TO = 269;
omega_LO = 293;
Gamma = 2.3;
a = 14.67;
b = 0.227;
gamma = a*omega_p^b; % curve fitting result
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)/(omega_TO^2-omega^2+1i*omega*Gamma)-...
    omega_p^2/(omega*(omega-1i*gamma)));
re = real(eps);
im = -imag(eps);
end