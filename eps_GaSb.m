% Author: Kaijun Feng
% Date: 6/18/2015
% Notes: this funciton is used to calculate the permittivity of GaP. Plasma term is not added in the
% model yet.
% Ref: Phys. Rev. B 11, 1587 (1975); Sol. Stat. Commun. 104, 747 (1997)
function [re, im]=eps_GaSb(omega, n)
omega_LO = 236; % 1/cm
omega_TO = 225;
% gamma_L = 1.18;
% gamma_T = 2.59;
gamma = 1;
eps_inf = 14.4;
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega^2+1i*omega*gamma));
% Factorial model
% eps = eps_inf*(omega_LO^2-omega^2+1i*gamma_L*omega)/(omega_TO^2-omega^2+1i*gamma_T*omega);
re = real(eps);
im = -imag(eps);
end