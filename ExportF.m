% Author: Kaijun Feng
% Date: 5/28/2015
% Notes: This function is used to generate the permitivitty data for given
% materials, with the auxiliary functions for each material.
function ExportF(handles, fnames, single)
%% set up
global xmin xmax xtype rho
N=500;
c = 2.998e10; %cm/s
x = linspace(xmin,xmax,N);
switch xtype
    case 1 % wavenumber
        omega = x;
    case 2 % wavelength
        f = 1e4*c./x;
        omega = f/c;
    case 3 % frequency in THz
        omega = 1e12 * x/c;
end
hwb = waitbar(0,'Calculating...','Name','Perm rocks');
%% Calculating and exporting permittivity data
funs = {'eps_InGaAs', 'GaN14eps_doped','eps_GaAs', 'eps_AlInAs', 'eps_InP',...
    'eps_AlN','eps_GaP','eps_cBN','eps_GaSb','eps_InSb','eps_InAs','eps_AlAs'...
    'eps_glass','eps_Ag','eps_Ge','eps_SiC','eps_Au'};
funs_uni = {'eps_sapphire_o','eps_sapphire_e','eps_composite_o','eps_composite_e'};
advanced = get(handles.tog1, 'Value');
uni = get(handles.radio_uni,'Value');
if uni
    eps_o_tmp = zeros(2,N);
    eps_e_tmp = zeros(2,N);
    n = get(handles.slider1,'Value');%cm^-3
    identifier = get(handles.pop_uni, 'Value');
    for k=1:N
        [eps_o_tmp(1,k), eps_o_tmp(2,k)]=feval(funs_uni{2*identifier-1}, omega(k), n);
        [eps_e_tmp(1,k), eps_e_tmp(2,k)]=feval(funs_uni{2*identifier}, omega(k), n);
        waitbar(k/N,hwb);
    end
    eps_o = eps_o_tmp(1,:) + 1i*eps_o_tmp(2,:);
    eps_e = eps_e_tmp(1,:) + 1i*eps_e_tmp(2,:);
    if advanced % 2-fold EMA
        eps_o_uni = eps_o;
        eps_e_uni = eps_e;
        eps2 = zeros(2,N);
        n2 = get(handles.edit2,'String');
        n2 = str2double(n2);
        identifier2 = get(handles.pop2, 'Value');
        for k=1:N
            [eps2(1,k), eps2(2,k)]=feval(funs{identifier2}, omega(k), n2);
            waitbar(k/N,hwb);
        end
        eps2_temp=eps2(1,:)+1i*eps2(2,:);
        eps_o=(1-rho)*eps_o_uni+rho*eps2_temp;
        eps_e=1./(rho./eps2_temp+(1-rho)./eps_e_uni);
    end    
    epsR_o = [x' real(eps_o)'];
    epsI_o = [x' imag(eps_o)'];
    epsR_e = [x' real(eps_e)'];
    epsI_e = [x' imag(eps_e)'];
    if single
        M = [x' epsR_o(:,2) epsI_o(:,2) epsR_o(:,2) epsI_o(:,2) epsR_e(:,2) epsI_e(:,2)];
        dlmwrite(fnames{:},M,' ');
    else
        dlmwrite(fnames{1},epsR_e,' ');
        dlmwrite(fnames{2},epsI_e,' ');
        dlmwrite(fnames{3},epsR_o,' ');
        dlmwrite(fnames{4},epsI_o,' ');
    end
elseif advanced 
    eps1 = zeros(2,N);
    eps2 = zeros(2,N);
    n1 = get(handles.slider1,'Value');%cm^-3
    n2str = get(handles.edit2,'String');
    n2 = str2double(n2str);
    identifier1 = get(handles.pop1, 'Value');
    identifier2 = get(handles.pop2, 'Value');
    for k=1:N
        [eps1(1,k), eps1(2,k)]=feval(funs{identifier1}, omega(k), n1);
        [eps2(1,k), eps2(2,k)]=feval(funs{identifier2}, omega(k), n2);
        waitbar(k/N,hwb);
    end
    eps1_temp=eps1(1,:)+1i*eps1(2,:);
    eps2_temp=eps2(1,:)+1i*eps2(2,:);
%     eps_o=(eps1_temp+eps2_temp)/2;
%     eps_e=2*eps1_temp.*eps2_temp./(eps1_temp+eps2_temp);
    eps_o=(1-rho)*eps1_temp+rho*eps2_temp;
    eps_e=1./(rho./eps2_temp+(1-rho)./eps1_temp);
    epsR_o = [x' real(eps_o)'];
    epsI_o = [x' imag(eps_o)'];
    epsR_e = [x' real(eps_e)'];
    epsI_e = [x' imag(eps_e)'];
    if single
        M = [x' epsR_o(:,2) epsI_o(:,2) epsR_o(:,2) epsI_o(:,2) epsR_e(:,2) epsI_e(:,2)];
        dlmwrite(fnames{:},M,' ');
    else
        dlmwrite(fnames{1},epsR_e,' ');
        dlmwrite(fnames{2},epsI_e,' ');
        dlmwrite(fnames{3},epsR_o,' ');
        dlmwrite(fnames{4},epsI_o,' ');
    end
else        
    eps = zeros(2,N);
    identifier = get(handles.pop1, 'Value');
    n = get(handles.slider1,'Value');
    for k=1:N
        [eps(1,k), eps(2,k)]=feval(funs{identifier}, omega(k), n);
        waitbar(k/N,hwb);
    end
    epsR = [x' eps(1,:)'];
    epsI = [x' eps(2,:)'];
    if single
        M = [x' epsR(:,2) epsI(:,2)];
        dlmwrite(fnames{:}, M, ' ');
    else
        dlmwrite(fnames{1}, epsR,' ');
        dlmwrite(fnames{2}, epsI,' ');
    end
end
close(hwb);
end