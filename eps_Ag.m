% Author: Kaijun Feng
% Date: 3/9/2016
% Notes: this funciton is used to calculate the permittivity of silver. 
% Ref: Nat. Photon. 6, 450 (2012)
function [re, im]=eps_Ag(omega, n)
eps_inf = 5;
gamma = 269; %1/cm
omega_p = 73251; %1/cm
eps = eps_inf-omega_p^2/(omega^2-1i*omega*gamma);
re = real(eps);
im = -imag(eps);
end