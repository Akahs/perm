% MIT License
% 
% Copyright (c) 2016 Kaijun Feng
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated docu5mentation files (the "sSoftware"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
function varargout = PermTool_v1(varargin)
% PERMTOOL_V1 MATLAB code for PermTool_v1.fig
%      PERMTOOL_V1, by itself, creates a new PERMTOOL_V1 or raises the existing
%      singleton*.
%
%      H = PERMTOOL_V1 returns the handle to a new PERMTOOL_V1  or the handle to
%      the existing singleton*.
%
%      PERMTOOL_V1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PERMTOOL_V1.M with the given input arguments.
%
%      PERMTOOL_V1('Property','Value',...) creates a new PERMTOOL_V1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PermTool_v1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PermTool_v1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PermTool_v1

% Last Modified by GUIDE v2.5 02-Mar-2018 14:52:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PermTool_v1_OpeningFcn, ...
                   'gui_OutputFcn',  @PermTool_v1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PermTool_v1 is made visible.
function PermTool_v1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PermTool_v1 (see VARARGIN)

% Choose default command line output for PermTool_v1
handles.output = hObject;

% Update handles structure
global xmin xmax xtype rho
xmin = 600;
xmax = 1400;
xtype = 1;
rho = 0.5;
guidata(hObject, handles);
set(handles.pop1, 'Value',1);
set(handles.grid,'Checked','off');
set(handles.wavenum,'Checked','on');
set(handles.radio_iso,'Value',1);
set(handles.pop1,'Visible','on');
set(handles.pop_uni,'Visible','off');
set(handles.tog1,'Enable','on');
set(handles.realPart,'Checked','on');
set(handles.imagPart,'Checked','on');
set(handles.ordPart,'Checked','on');
set(handles.extPart,'Checked','on');
PermF(handles);


% UIWAIT makes PermTool_v1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PermTool_v1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in pop1.
function pop1_Callback(hObject, eventdata, handles)
% hObject    handle to pop1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop1
option = get(hObject,'Value');
if option <= 3
    set(handles.slider1,'Enable','on')
    set(handles.edit1,'Enable','on');
else
    set(handles.slider1,'Enable','off')
    set(handles.edit1,'Enable','off')
end
PermF(handles);


% --- Executes during object creation, after setting all properties.
function pop1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tog1.
function tog1_Callback(hObject, eventdata, handles)
% hObject    handle to tog1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tog1
advanced = get(handles.tog1,'Value');
if advanced
    set(handles.uipanel1,'Visible','on')
    PermF(handles)
else
    set(handles.uipanel1,'Visible','off')
    PermF(handles)
end


% --- Executes on selection change in pop2.
function pop2_Callback(hObject, eventdata, handles)
% hObject    handle to pop2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop2
option = get(hObject,'Value');
if option <= 3
%     set(handles.slider2,'Enable','on')
    set(handles.edit2,'Enable','on')
else
%     set(handles.slider2,'Enable','off')
    set(handles.edit2,'Enable','off')
end
PermF(handles);


% --- Executes during object creation, after setting all properties.
function pop2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
temp=get(hObject,'Value');
doping=num2str(temp,3);
set(handles.edit1,'String',doping)
PermF(handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
temp=get(handles.edit1,'String');
doping = str2double(temp);
set(handles.slider1,'Value',doping);
PermF(handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
temp=get(hObject,'Value');
doping=num2str(temp,3);
set(handles.edit2,'String',doping)
PermF(handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
temp = get(hObject,'String');
doping = str2double(temp);
% set(handles.slider2,'Value',doping);
PermF(handles);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function xlim_Callback(hObject, eventdata, handles)
% hObject    handle to xlim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax
temp = inputdlg({'xmin','xmax'});
if ~isempty(temp)
    xmin=str2double(temp(1));
    xmax=str2double(temp(2));
    PermF(handles);
end

% --------------------------------------------------------------------
function grid_Callback(hObject, eventdata, handles)
% hObject    handle to grid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    grid(handles.axes1,'off')
else
    set(hObject,'Check','on')
    grid(handles.axes1,'on')
end


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function none_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function export_Callback(hObject, eventdata, handles)
% hObject    handle to none (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
name = 'File names';
numlines = 1;
options.Interpreter = 'latex';
options.Resize = 'on';
meta = get(handles.tog1,'Value');
uni = get(handles.radio_uni,'Value');
if meta || uni
    prompt={'File name for real($\varepsilon_e$):',...
        'File name for imag($\varepsilon_e$):',...
        'File name for real($\varepsilon_o$):',...
        'File name for imag($\varepsilon_o$):'};
    default = {'epsR_e.txt','epsI_e.txt','epsR_o.txt','epsI_o.txt'};
    answer = inputdlg(prompt, name, numlines, default, options);
else
    prompt = {'Filename for real($\varepsilon$):',...
        'Filename for imag($\varepsilon$):'};
    default = {'epsR.txt','epsI.txt'};
    answer = inputdlg(prompt, name, numlines, default, options);
end
if ~isempty(answer)
    ExportF(handles, answer, false)
end


% --------------------------------------------------------------------
function xaxis_Callback(hObject, eventdata, handles)
% hObject    handle to xaxis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function wavenum_Callback(hObject, eventdata, handles)
% hObject    handle to wavenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax xtype
if xtype~=1
    set(hObject,'Checked','on');
    c = 2.998e10; % cm/s
    if xtype == 2 % wavelength
        set(handles.lambda,'Checked','off');
        temp = xmin;
        xmin = 1e4/xmax;
        xmax = 1e4/temp;
    else % frequency
        set(handles.freq,'Checked','off');
        xmin = 1e12 * xmin/c;
        xmax = 1e12 * xmax/c;
    end
    xtype=1;
    PermF(handles);
end

    


% --------------------------------------------------------------------
function lambda_Callback(hObject, eventdata, handles)
% hObject    handle to lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax xtype
if xtype~=2
    set(hObject,'Checked','on');    
    c = 2.998e10; % cm/s
    if xtype == 1 % wavenumber
        set(handles.wavenum,'Checked','off');
        temp = xmin;
        xmin = 1e4/xmax;
        xmax = 1e4/temp;
    else % frequency
        set(handles.freq,'Checked','off');
        temp = xmin;
        xmin = 1e-8*c/xmax;
        xmax = 1e-8*c/temp;
    end    
    xtype=2;
    PermF(handles);
end


% --------------------------------------------------------------------
function freq_Callback(hObject, eventdata, handles)
% hObject    handle to freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax xtype
if xtype~=3 % Unit: THz
    set(hObject,'Checked','on');
    c = 2.998e10; % cm/s
    if xtype == 1 % wavenumber
        set(handles.wavenum,'Checked','off');
        xmin = xmin * c * 1e-12;
        xmax = xmax * c * 1e-12;
    else % wavelength
        set(handles.lambda,'Checked','off');
        temp = xmin;
        xmin = 1e-8*c/xmax;
        xmax = 1e-8*c/temp;
    end
    xtype = 3;
    PermF(handles);
end


% --- Executes on selection change in pop_uni.
function pop_uni_Callback(hObject, eventdata, handles)
% hObject    handle to pop_uni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_uni contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_uni
PermF(handles);


% --- Executes during object creation, after setting all properties.
function pop_uni_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_uni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radio_uni.
function radio_uni_Callback(hObject, eventdata, handles)
% hObject    handle to radio_uni (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_uni
state = get(hObject, 'Value');
if state
    set(handles.pop_uni,'Visible','On');
    set(handles.pop1,'Visible','Off');
    set(handles.tog1,'Value',0);
%     set(handles.tog1,'Enable','off');
    set(handles.uipanel1,'Visible','off');
    set(handles.slider1,'Enable','off');
    set(handles.edit1,'Enable','off');
    PermF(handles);
end


% --- Executes on button press in radio_iso.
function radio_iso_Callback(hObject, eventdata, handles)
% hObject    handle to radio_iso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_iso
state = get(hObject, 'Value');
if state
    set(handles.pop_uni,'Visible','Off');
    set(handles.pop1,'Visible','On');
    set(handles.tog1,'Enable','on');
%     set(handles.uipanel1,'Visible','on');
    set(handles.slider1,'Enable','on');
    set(handles.edit1,'Enable','on');
    PermF(handles);
end



function editRho_Callback(hObject, eventdata, handles)
% hObject    handle to editRho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editRho as text
%        str2double(get(hObject,'String')) returns contents of editRho as a double
global rho
rho = str2double(get(hObject,'String'));
PermF(handles);


% --- Executes during object creation, after setting all properties.
function editRho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editRho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function composite_Callback(hObject, eventdata, handles)
% hObject    handle to composite (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_7_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ordPart_Callback(hObject, eventdata, handles)
% hObject    handle to ordPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    PermF(handles);
else
    set(hObject,'Check','on')
    PermF(handles);
end


% --------------------------------------------------------------------
function extPart_Callback(hObject, eventdata, handles)
% hObject    handle to extPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    PermF(handles);
else
    set(hObject,'Check','on')
    PermF(handles);
end


% --------------------------------------------------------------------
function realPart_Callback(hObject, eventdata, handles)
% hObject    handle to realPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    PermF(handles);
else
    set(hObject,'Check','on')
    PermF(handles);
end


% --------------------------------------------------------------------
function imagPart_Callback(hObject, eventdata, handles)
% hObject    handle to imagPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    PermF(handles);
else
    set(hObject,'Check','on')
    PermF(handles);
end


% --------------------------------------------------------------------
function export_single_Callback(hObject, eventdata, handles)
% hObject    handle to export_single (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
name = 'File names';
numlines = 1;
% options.Interpreter = 'latex';
options.Resize = 'on';
% meta = get(handles.tog1,'Value');
% uni = get(handles.radio_uni,'Value');
% if meta || uni
%     prompt={'File name for real($\varepsilon_e$):',...
%         'File name for imag($\varepsilon_e$):',...
%         'File name for real($\varepsilon_o$):',...
%         'File name for imag($\varepsilon_o$):'};
%     default = {'epsR_e.txt','epsI_e.txt','epsR_o.txt','epsI_o.txt'};
%     answer = inputdlg(prompt, name, numlines, default, options);
% else
%     prompt = {'Filename for real($\varepsilon$):',...
%         'Filename for imag($\varepsilon$):'};
%     default = {'epsR.txt','epsI.txt'};
%     answer = inputdlg(prompt, name, numlines, default, options);
% end
prompt = 'File name';
default = {'eps.txt'};
answer = inputdlg(prompt, name, numlines, default, options);
if ~isempty(answer)
    ExportF(handles, answer, true)
end
