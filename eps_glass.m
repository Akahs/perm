% Author: Kaijun Feng
% Date: 12/11/2015
% Note: permittivity for silica glass, based on two models
% Ref: [1] R. Kitamura, et al. App. Opt. 46, pp. 8118 (2007)
% [2] D. De Sousa Meneses, et al. J. Non-Cryst. Sol. 352, pp. 769 (2006)
function [re, im] = eps_glass (omega,n)
lambda = 1e4/omega;
if lambda < 7
    re = 1 + 0.6961663*lambda^2/(lambda^2-0.0684043^2) + ...
        0.4079426 * lambda^2/(lambda^2-0.1162414^2) + ...
        0.8974794 * lambda^2 / (lambda^2-9.896161^2);
    im = 0;
else
    alpha = [3.7998, 0.46089, 1.2520, 7.8147, 1.0313, 5.3757, 6.3305, 1.2948];
    eta = [1089.7, 1187.7, 797.78, 1058.2, 446.13, 443.00, 465.80, 1026.7];
    sigma = [31.454, 100.46, 91.601, 63.153, 275.111, 45.220, 22.680, 232.14];
    g = alpha .* exp(-4 * log(2)*((omega - eta)./sigma).^2) - ...
        alpha .* exp(-4 * log(2)*((omega + eta)./sigma).^2);
%     fun = @(t) exp(t.^2);
%     syms t;
%     D = @(x) exp(-x.^2).*int(exp(t^2),[0,x]);
%     g_kkg = 2*alpha/pi .* (feval('D',2*sqrt(log(2))*(omega+eta)./sigma) - feval('D',2*sqrt(log(2))*(omega-eta)./sigma));
    g_kkg = 2*alpha/pi .* (dawson(2*sqrt(log(2))*(omega+eta)./sigma)-dawson(2*sqrt(log(2))*(omega-eta)./sigma));
    eps_inf = 2.1232;
    eps = eps_inf + sum(g_kkg) + 1i * sum(g);
    re = real(eps);
    im = imag(eps);
end