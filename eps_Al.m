% Author: Kaijun Feng
% Date: 4/17/2018
% Notes: this funciton is used to calculate the permittivity of aluminum. 
% Ref: Handbook of Optical Constants of Solids (1998)
function [re, im]=eps_Al(omega, n)
hc = 1.24e-4; % eV*cm
omega = hc * omega; % eV
eps_inf = 1.03;
gamma = 0.1; % eV
omega_p = 13; % eV
eps = eps_inf-omega_p^2/(omega^2-1i*omega*gamma);
re = real(eps);
im = -imag(eps);
end