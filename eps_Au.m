% Author: Kaijun Feng
% Date: 3/7/2017
% Notes: this funciton is used to calculate the permittivity of gold; valid upto the visible range. 
% Ref: Chem. Phys. Lett. 399, 167 (2004);
function [re, im]=eps_Au(omega, n)
eps_inf = 9.5;
gamma = 557; %1/cm
omega_p = 72150; %1/cm
eps = eps_inf-omega_p^2/(omega^2-1i*omega*gamma);
re = real(eps);
im = -imag(eps);
end